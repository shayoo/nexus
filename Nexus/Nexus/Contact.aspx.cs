﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace Nexus
{
    public partial class Contact : System.Web.UI.Page
    {
        private SqlConnection xConn;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);

        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            txtuname.Text = "";
            txtemail.Text = "";
            txtphone.Text = "";
            txtmess.Text = "";
        }

        protected void btnsend_Click(object sender, EventArgs e)
        {
            try
            {
                xConn.Open();
                SqlCommand xcomd = new SqlCommand("insert into tblcontact values('" + txtuname.Text + "','" + txtemail.Text + "','" + txtphone.Text + "','" + txtmess.Text + "','true')", xConn);
                xcomd.ExecuteNonQuery();
                xcomd.Dispose();
                xConn.Close();
                txtuname.Text = "";
                txtemail.Text = "";
                txtphone.Text = "";
                txtmess.Text = "";
            }
            catch (SqlException s)
            {
                Response.Write("<script>alert('Please enter correctly')</script>");
                txtuname.Text = "";
                txtemail.Text = "";
                txtphone.Text = "";
                txtmess.Text = "";
            }
        }
    }
}
