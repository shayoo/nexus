﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
namespace Nexus
{
    public partial class UHome : System.Web.UI.Page
    {
    private SqlConnection xConn;
    private SqlDataAdapter xDataAdapter;
    private DataSet xDataSet;
        
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["u"] == null)
        {
            Response.Redirect("Sorry.aspx");
        }
        else
        {

        }
        //ScriptManager.RegisterStartupScript(this, GetType(), "", "$.fancybox.close();", true); 
        Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); parent.location ='UHome.aspx'; },0)</script>");
        string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
        xConn = new SqlConnection(sString);
        xConn.Open();
        SqlCommand cmd = new SqlCommand("select Fname from tblUser where U_ID='" + Session["u"].ToString() + "' and Status='true'", xConn);
        cmd.ExecuteNonQuery();
        SqlDataReader ds;
        ds = cmd.ExecuteReader();
        while (ds.Read())
        {
            la1.Text = ds[0].ToString();
        }
        ds.Close();
        cmd.Dispose();
        xConn.Close();
        l3.Text = DateTime.Today.ToString("MMM");
        DataSet xDataSet3 = new DataSet();
        SqlDataAdapter xDataAdapter3 = new SqlDataAdapter("select Fname as FirstName,Lname as LastName,Bill,Services,Package,Category,Payment from tblUser where Status='true' and U_ID='" + Session["u"].ToString() + "'", xConn);
        xDataAdapter3.Fill(xDataSet3);
        DGV1.DataSource = xDataSet3.Tables[0];
        DGV1.DataBind();
        DGV1.Dispose();
        //xDataSet = new DataSet();
        //xDataAdapter = new SqlDataAdapter("select PID,Username,Bill,Rupees_in_words,Mode_of_Payment,Date from tblpayment where Status='true' and U_ID='"+Session["un"].ToString()+"'", xConn);
        //xDataAdapter.Fill(xDataSet);
        //DGV1.DataSource = xDataSet.Tables[0];
        //DGV1.DataBind();
        
    }

    protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        Session.Abandon();
    }
    ////private string ConvertSortDirectionToSql(SortDirection sortDirection)
    ////{
    ////    string newSortDirection = String.Empty;

    ////    switch (sortDirection)
    ////    {
    ////        case SortDirection.Ascending:
    ////            newSortDirection = "ASC";
    ////            break;

    ////        case SortDirection.Descending:
    ////            newSortDirection = "DESC";
    ////            break;
    ////    }

    ////    return newSortDirection;
    ////}
    //protected void DGV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    DGV1.PageIndex = e.NewPageIndex;
    //    DGV1.DataBind();
    //}

    //protected void DGV1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dataTable = DGV1.DataSource as DataTable;

    //    if (dataTable != null)
    //    {
    //        DataView dataView = new DataView(dataTable);
    //        dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

    //        DGV1.DataSource = dataView;
    //        DGV1.DataBind();
    //    }
    //}
    }
}
