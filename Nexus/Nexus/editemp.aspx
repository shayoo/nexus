﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="editemp.aspx.cs" Inherits="Nexus.editemp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Editing Employee Details</h3>
    <table style="width: 553px; margin-left:140px; height: 84px; ">
            
             <tr>
    <td class="style1">EmpID:</td>
    <td>
                    <asp:TextBox ID="txtUID" runat="server" TextMode="SingleLine"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
            ControlToValidate="txtUID" Display="Static" ErrorMessage="*"></asp:RequiredFieldValidator>
     </td>
    </tr>
       
             <tr>
    <td class="style1">FirstName : </td>
    <td><asp:TextBox ID="txtfname" runat="server"></asp:TextBox>
     <asp:RequiredFieldValidator ID="RFV1" ControlToValidate="txtfname" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
      <asp:RegularExpressionValidator ID="REV1" runat="server" ValidationExpression="^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$" ControlToValidate="txtfname" ErrorMessage="Invalid FName Format"></asp:RegularExpressionValidator> 
     </td>
    </tr>
       
    <tr>
    <td class="style1">LastName :</td>
    <td><asp:TextBox ID="txtlname" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RFV2" runat="server" 
            ControlToValidate="txtlname" Display="Static" ErrorMessage="*"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="REV2" runat="server" 
            ControlToValidate="txtlname" ErrorMessage="Invalid lName Format" 
            ValidationExpression="^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$"></asp:RegularExpressionValidator>
    </td>
    </tr>
    
<tr>
                <td class="style1">
                    Phone Number:</td>
                <td>
                <asp:TextBox ID="txtphone" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ControlToValidate="txtphone" Display="Static" ErrorMessage="*"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
            ControlToValidate="txtphone" ErrorMessage="Invalid Phone Format" 
            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>    
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Address:</td>
                <td>
                    <asp:TextBox ID="txtadd" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="txtadd" Display="Static" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>

    <tr>
    <td class="style1">Email:</td>
    <td><asp:TextBox ID="txtemail" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
            ControlToValidate="txtemail" Display="Static" ErrorMessage="*"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="r" runat="server" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ControlToValidate="txtemail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>             
    </td>
    </tr>
    
   
    <tr>
    <td class="style1">Salary :</td>
    <td><asp:TextBox ID="txtbill" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RFV5" ControlToValidate="txtbill" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="REV5" runat="server" ValidationExpression="^\d+$" ControlToValidate="txtbill" ErrorMessage="Invalid bill Format"></asp:RegularExpressionValidator>
     </td>
    </tr>
    
   
    <tr>
    <td class="style1">Designation:</td>
    <td><asp:TextBox ID="txtdes" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtdes" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
      <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$" ControlToValidate="txtdes" ErrorMessage="Invalid Designation Format"></asp:RegularExpressionValidator> 
    </td>
    </tr>
            <tr>
                <td class="style1">
                    Select Franchise:</td>
                <td>
                    <asp:DropDownList ID="dll1" runat="server" Height="28px" Width="189px">
                        <asp:ListItem> :: Select Franchise ::</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="l1" runat="server"></asp:Label>
                </td>
            </tr>
    
    
    
    
    <%--<tr>
    <td>Date :</td>
    <td><asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RFV8" ControlToValidate="txtDate" ErrorMessage="*" Display="Static" runat="server"></asp:RequiredFieldValidator>                
    <asp:RegularExpressionValidator ID="REV6" runat="server" ValidationExpression="^(((0[1-9]|[12]\d|3[01])-(0[13578]|1[02])-((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)-(0[123456789]|1[012])-((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$" ControlToValidate="txtDate" ErrorMessage="Invalid Date Format"></asp:RegularExpressionValidator>
     </td>
    </tr>
--%>
    
    <tr>
    <td class="style1"> &nbsp;<td><asp:Button ID="btnSave" Text="Save" runat="server" 
            onclick="btnSave_Click" />
    &nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button1" Text="Clear" runat="server" onclick="Button1_Click" /> 
        <p>
            &nbsp;</p>
        </td>
   </tr>

        </table>

    </div>
    </form>
</body>
</html>
