﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iadd.aspx.cs" Inherits="Nexus.iadd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            height: 31px;
        }
        .style3
        {
            height: 77px;
        }
        .style5
        {
            height: 21px;
        }
        .style6
        {
            height: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Add Package</h3>
    <table style="width: 479px; margin-left:140px; height: 164px;">
            <tr>
                <td class="style5">
                    Service:</td>
                <td class="style5">
                    <asp:DropDownList ID="dll1" runat="server" Height="28px" Width="189px">
                        <asp:ListItem> :: Select Service ::</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="l1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    Add Package:</td>
                <td class="style5">
                    <asp:TextBox ID="txtpk" runat="server" Width="189px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFV1" runat="server" ControlToValidate="txtpk" 
                        Display="Static" ErrorMessage="*"></asp:RequiredFieldValidator>
                    
                </td>
            </tr>
            <tr>
                <td class="style5">
                    Package type:</td>
                <td class="style5">
                    <asp:TextBox ID="txtpt" runat="server" Width="187px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFV2" runat="server" ControlToValidate="txtpt" 
                        Display="Static" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    Package Validity:</td>
                <td class="style5">
                    <asp:TextBox ID="txtpv" runat="server" Width="188px"></asp:TextBox>
                    
                </td>
            </tr>
            <tr>
                <td class="style3">
                    Description:</td>
                <td class="style3">
                    <asp:TextBox ID="txtadd" runat="server" Width="270px" TextMode="MultiLine" 
                        Height="71px"></asp:TextBox>
                    
                </td>
            </tr>
            <tr>
                <td class="style6">
                    Image(Optional):</td>
                <td class="style6">
                    
                    <asp:FileUpload ID="UFile" runat="server" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UFile"
ErrorMessage="Invalid Image File"
ValidationExpression=
"^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.bmp|.BMP|.gif|.GIF|.png|.PNG)$">
</asp:RegularExpressionValidator> 

                </td>
            </tr>
            <tr>
                <td class="style1">
                    </td>
                <td class="style1">
                    <asp:Button ID="btnsave" runat="server" onclick="btnsave_Click" Text="Save" />
&nbsp;
                    <asp:Button ID="btnclear" runat="server" onclick="btnclear_Click" 
                        Text="Clear" style="height: 26px" />
                </td>
            </tr>
        </table>
        </div>
    </form>
</body>
</html>
