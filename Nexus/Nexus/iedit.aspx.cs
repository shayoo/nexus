﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
namespace Nexus
{
    public partial class iedit : System.Web.UI.Page
    {
        private SqlConnection xConn;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["a"] == null)
            {
                Response.Redirect("Sorry.aspx");
            }
            else
            {

            }
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);
            Show();
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (dll1.SelectedIndex.Equals(0))
            {
                l1.ForeColor = System.Drawing.Color.Red;
                l1.Text = "You Should Select Service!";
                
            }
            else if (ddl2.SelectedIndex.Equals(0))
            {
                l2.ForeColor = System.Drawing.Color.Red;
                l2.Text = "You Should Select Package!";
            }
            else
            {
                if (UFile.HasFile)
                {
                    try
                    {

                        string FName = UFile.FileName.ToString();
                        //int FSize = UFile.PostedFile.ContentLength;
                        UFile.PostedFile.SaveAs(Server.MapPath("Images/" + FName));
                        string NewFName = txtpk.Text;
                        System.IO.File.Move(Server.MapPath("Images/" + FName), Server.MapPath("Images/" + NewFName));
                        //                IMG1.ImageUrl = "Images/" + NewFName;

                        xConn.Open();
                        SqlCommand xcomd = new SqlCommand("update tblinternet set Package='" + txtpk.Text + "',types='" + txtpt.Text + "',validity='" + txtpv.Text + "',Description='" + txtadd.Text + "',Images='" + NewFName + "' where Services='" + dll1.SelectedItem.Text.ToString() + "' and Package='" + ddl2.SelectedItem.Text.ToString()+"'", xConn);
                        xcomd.ExecuteNonQuery();
                        xcomd.Dispose();
                        xConn.Close();
                        Response.Write("<script>alert('Updated!')</script>");
                        Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); },0)</script>");
                        dll1.SelectedIndex=0;
                        txtpk.Text = "";
                        txtadd.Text = "";
                        
                    }
                    catch (SqlException s)
                    {
                        Response.Write("<script>alert('Package having this name is already exist!')</script>");
                        dll1.SelectedIndex = 0;
                        txtpk.Text = "";
                        txtadd.Text = "";

                    }
                    catch (IOException ie)
                    {
                        Response.Write("<script>alert('A file contaning this name already exist!')</script>");
                        txtpk.Text = "";
                    }
                }
                else
                {
                    try
                    {
                    xConn.Open();
                    SqlCommand xcomd = new SqlCommand("update tblinternet set Package='" + txtpk.Text + "',types='" + txtpt.Text + "',validity='" + txtpv.Text + "',Description='" + txtadd.Text + "' where Services='" + dll1.SelectedItem.Text.ToString() + "' and Package='" + ddl2.SelectedItem.Text.ToString() + "'", xConn);
                        xcomd.ExecuteNonQuery();
                        xcomd.Dispose();
                        xConn.Close();
                        Response.Write("<script>alert('Updated!')</script>");
                        Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); },0)</script>");
                        dll1.SelectedIndex = 0;
                        ddl2.SelectedIndex = 0;
                        txtpk.Text = "";
                        txtadd.Text = "";
                        txtpt.Text = "";
                        txtpv.Text = "";
                        Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); parent.location ='AHome.aspx'; },0)</script>");
                    }
                    catch (SqlException s)
                    {
                        Response.Write("<script>alert('Package having this name is already exist!')</script>");
                        dll1.SelectedIndex = 0;
                        ddl2.SelectedIndex = 0;
                        txtpk.Text = "";
                        txtadd.Text = "";
                        txtpt.Text = "";
                        txtpv.Text = "";
                    }
                }
            }
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            dll1.SelectedIndex = 0;
            ddl2.SelectedIndex = 0;
            txtpk.Text = "";
            txtadd.Text = "";
            txtpt.Text = "";
            txtpv.Text = "";

        }
        protected void Show()
        {


            try
            {
                xConn.Open();
                //DataTable dr = new DataTable();

                SqlCommand cmd = new SqlCommand("select Services from tblservices where Status='true'", xConn);
                //cmd.Fill(dr);
                //cmd.CommandType = CommandType.Text;

                SqlDataReader ds;
                ds = cmd.ExecuteReader();
                while (ds.Read())
                {
                    dll1.Items.Add(ds[0].ToString());
                }
                //ListItem li = new ListItem();
                //for (int i = 0; i <= str; i++)
                //{
                //  li.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                //li.Value = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                //dll1.DataSource = ds;
                //dll1.DataTextField = "Karachi";
                //dll1.DataValueField = "Karachi";
                //dll1.Items.Add(li);
                //}
                //dll1.DataBind();
                //dr.Clear();
                cmd.Dispose();
                ds.Close();
                xConn.Close();


            }
            catch (IndexOutOfRangeException se)
            {
                Response.Write("<script>alert('Please enter correctly')</script>");
                Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); parent.location ='AHome.aspx'; },0)</script>");
            }
            catch (SqlException sq)
            {
                Response.Write("<script>alert('Please enter correctly')</script>");
            }
            //dll1.Items.Insert(0, new ListItem("    :: Select Franchise ::   ", "0"));
        }

        protected void dll1_SelectedIndexChanged(object sender, EventArgs e)
        {
                
                try
                {
                    xConn.Open();
                    //DataTable dr = new DataTable();

                    SqlCommand cmd = new SqlCommand("select Package from tblinternet where Status='true' and Services='" + dll1.SelectedItem.Text.ToString() + "'", xConn);
                    //cmd.Fill(dr);
                    //cmd.CommandType = CommandType.Text;

                    SqlDataReader ds;
                    ds = cmd.ExecuteReader();
                    while (ds.Read())
                    {
                        ddl2.Items.Add(ds[0].ToString());
                    }
                    //ListItem li = new ListItem();
                    //for (int i = 0; i <= str; i++)
                    //{
                    //  li.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                    //li.Value = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                    //dll1.DataSource = ds;
                    //dll1.DataTextField = "Karachi";
                    //dll1.DataValueField = "Karachi";
                    //dll1.Items.Add(li);
                    //}
                    //dll1.DataBind();
                    //dr.Clear();
                    cmd.Dispose();
                    ds.Close();
                    xConn.Close();


                }
                catch (IndexOutOfRangeException se)
                {
                    Response.Write("<script>alert('Please enter correctly')</script>");
                    Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); parent.location ='AHome.aspx'; },0)</script>");
                }
                catch (SqlException sq)
                {
                    Response.Write("<script>alert('Please enter correctly')</script>");
                }
                //dll1.Items.Insert(0, new ListItem("    :: Select Franchise ::   ", "0"));
            }


    }
}
