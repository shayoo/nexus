﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
namespace Nexus
{
    public partial class payment : System.Web.UI.Page
    {
        private SqlConnection xConn;
        private SqlDataAdapter xDataAdapter;
        private DataSet xDataSet;
        string str;
        string str2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["e"] == null)
            {
                Response.Redirect("Sorry.aspx");
            }
            else
            {

            }
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);
   ////         Image bitMapImage = new System.Drawing.Image(i1.Load);
   ////         Graphics graphicImage = Graphics.FromImage(bitMapImage);
   ////         graphicImage.SmoothingMode = SmoothingMode.AntiAlias;
   ////         graphicImage.DrawString("Shayan",
   ////new Font("Arial", 12, FontStyle.Bold),
   ////SystemBrushes.WindowText, new Point(50, 50));
            xConn.Open();
            SqlCommand cmd = new SqlCommand("select * from tblbill", xConn);
            cmd.ExecuteNonQuery();
            SqlDataReader ds;
            ds = cmd.ExecuteReader();
            while (ds.Read())
            {
                image1.ImageUrl ="images/" + ds[0].ToString();
            }
            ds.Close();
            cmd.Dispose();
            xConn.Close();
            txtfname.Enabled = false;
            txtbill.Enabled = false;
            txtriw.Enabled = false;
            txtmop.Enabled = false;
            btnsave.Enabled = false;
            txtnote.Enabled = false;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                xConn.Open();
                SqlCommand cmd = new SqlCommand("select Fname,Lname from tblUser where U_ID='" + txtUID.Text + "' and Status='true'", xConn);
                cmd.ExecuteNonQuery();
                SqlDataReader ds;
                ds = cmd.ExecuteReader();
                while (ds.Read())
                {
                    txtfname.Text = ds[0].ToString() + " " + ds[1].ToString();
                }
                ds.Close();
                cmd.Dispose();
                xConn.Close();

                txtUID.Enabled = false;
                txtbill.Enabled = true;
                txtnote.Enabled = true;
                txtriw.Enabled = true;
                txtmop.Enabled = true;
                btnsave.Enabled = true;
            }
            catch (SqlException s)
            {
                Response.Write("<script>alert('There is no U_ID like that!')</script>");
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {try
            {
            str2=DateTime.Today.ToString("dd/MM/yyyy");
            xConn.Open();
            SqlCommand cmd = new SqlCommand("insert into tblpayment values('"+txtUID.Text+"','"+txtfname.Text+"','"+txtbill.Text+"$','"+txtriw.Text+"','"+txtmop.Text+"','"+txtnote.Text+"','"+str2+"','true')", xConn);
            SqlCommand cmd2 = new SqlCommand("update tblUser set Bill='" + txtbill.Text + "', Payment='Paid' where U_ID='" + txtUID.Text + "' and Status='true'", xConn);
            cmd.ExecuteNonQuery();
            cmd2.ExecuteNonQuery();
            cmd.Dispose();
            cmd2.Dispose();
            
            
            lblTest.Text = txtfname.Text;

            SqlCommand cmd3 = new SqlCommand("select PID from tblpayment where UID='" + txtUID.Text + "' and Status='true'", xConn);
                cmd3.ExecuteNonQuery();
                SqlDataReader ds3;
                ds3 = cmd3.ExecuteReader();
                while (ds3.Read())
                {
                    Label1.Text = ds3[0].ToString();
                }
                ds3.Close();
                cmd3.Dispose();
                xConn.Close();
                Label2.Text = txtUID.Text;
                Label3.Text = str2;
                Label4.Text = txtriw.Text;
                Label5.Text = txtmop.Text;
                Label6.Text = txtnote.Text;
                txtfname.Text = "";
                txtbill.Text = "";
                txtriw.Text = "";
                txtmop.Text = "";
                txtnote.Text = "";
                txtfname.Enabled = false;
                txtbill.Enabled = false;
                txtriw.Enabled = false;
                txtmop.Enabled = false;
                btnsave.Enabled = false;
                txtnote.Enabled = false;
                txtUID.Text ="";
                txtUID.Enabled = true;
            }
        catch (SqlException s)
        {
            Response.Write("<script>alert('There is no U_ID like that!')</script>");
        }
        }
    }
}
