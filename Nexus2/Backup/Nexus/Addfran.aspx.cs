﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace Nexus
{
    public partial class Addfran : System.Web.UI.Page
    {
        private SqlConnection xConn;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["a"] == null)
            {
                Response.Redirect("Sorry.aspx");
            }
            else
            {

            }
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                xConn.Open();
                SqlCommand xcomd = new SqlCommand("insert into tblfranchise values('" + txtpk.Text + "','" + txtadd.Text + "','" + txtsup.Text + "','" + txtcn.Text + "','" + txtemail.Text + "','true')", xConn);
                xcomd.ExecuteNonQuery();
                xcomd.Dispose();
                xConn.Close();
                Response.Write("<script>alert('Data Saved!')</script>");
                Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); },0)</script>");
                txtpk.Text = "";
                txtadd.Text = "";
                txtsup.Text = "";
                txtcn.Text = "";
                txtemail.Text = "";
            }
            catch (SqlException sq)
            {
                Response.Write("<script>alert('Franchise Should be Unique!')</script>");
                txtpk.Text = "";
                txtadd.Text = "";
                txtsup.Text = "";
                txtcn.Text = "";
                txtemail.Text = "";
            }

        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            txtpk.Text = "";
            txtadd.Text = "";
            txtsup.Text = "";
            txtcn.Text = "";
            txtemail.Text = "";
        }
    }
}
