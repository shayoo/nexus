﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
namespace Nexus
{
    public partial class EHome : System.Web.UI.Page
    {
        private SqlConnection xConn;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["e"] == null)
            {
                Response.Redirect("Sorry.aspx");
            }
            else
            {

            }
            //ScriptManager.RegisterStartupScript(this, GetType(), "", "$.fancybox.close();", true); 
            Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); parent.location ='EHome.aspx'; },0)</script>");
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);
            xConn.Open();
            SqlCommand cmd = new SqlCommand("select Fname from tblemp where U_ID='"+Session["e"].ToString()+"' and Status='true'",xConn);
            cmd.ExecuteNonQuery();
            SqlDataReader ds;
            ds = cmd.ExecuteReader();
            while (ds.Read())
            {
                la1.Text = ds[0].ToString();
            }
            ds.Close();
            cmd.Dispose();
            xConn.Close();
            
        }

        protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Session.Abandon();
        }
    }
}
