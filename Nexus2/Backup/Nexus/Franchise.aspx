﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Franchise.aspx.cs" Inherits="Nexus.Franchise" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Franchise</title>
	<meta charset="utf-8">
    <meta http-equiv="Refresh" content="2" />
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">  
	<script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="js/cufon-yui.js" type="text/javascript"></script>
	<script src="js/cufon-replace.js" type="text/javascript"></script>
	<script src="js/Vegur_500.font.js" type="text/javascript"></script>
	<script src="js/Ropa_Sans_400.font.js" type="text/javascript"></script> 
	<script src="js/FF-cash.js" type="text/javascript"></script>	
	<script src="js/script.js" type="text/javascript"></script>  
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
		</a>
	</div>
	<![endif]-->
	<!--[if lt IE 9]>
 		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
	<![endif]-->
	
</head>
<body id="page5">
	<!--==============================header=================================-->
	<header>
		<div class="border-bot">
			<div class="main">
				<h1><a href="index.html">InternetCafe</a></h1>
				<nav>
					<ul class="menu">
						<li><a href="Default.aspx">Home</a></li>
						<%--<li><a href="index-1.html">Internet</a></li>--%>
						<li><a href="Services.aspx">Services</a></li>
						<li><a  class="active" href="Franchise.aspx">Franchise</a></li>
						<li><a href="Contact.aspx">Contacts</a></li>
					</ul>
				</nav>
				<div class="clear"></div>
			</div>
			</div>
	</header>
	<!--==============================content================================-->
	<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com - Mrach 03, 2012!</div>
						<form id="f3" runat="server">
                        
		<div class="main" style="height:800px;">
			<div class="container_12">
				<div class="wrapper">
					<article class="grid_8">
						<h3>Franchises</h3>
						                <asp:GridView ID="DGV1" runat="server" AllowPaging="True" CellPadding="4" 
                                            ForeColor="#333333" GridLines="None" Height="220px" HorizontalAlign="Center" 
                                            Width="956px" PageSize="20" AllowSorting="True" 
                            OnPageIndexChanging="gridView_PageIndexChanging" 
                            OnSorting="gridView_Sorting">
                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <RowStyle BackColor="#F7F6F3" HorizontalAlign="Center" ForeColor="#333333" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <EditRowStyle BackColor="#999999" />
                                            <AlternatingRowStyle BackColor="White" />
                                        </asp:GridView>
					</article>
					<article class="grid_4">
						<div class="indent-top indent-left">
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>
	<!--==============================footer=================================-->
	<footer>
			<div class="main">
			<div class="container_12">
				<div class="wrapper">
					<div class="grid_3">
						<div class="spacer-1">
							<a href="Default.aspx"><img src="images/nex5.jpg" alt=""></a>
						</div>
					</div>
					<div class="grid_5">
						<div class="indent-top2">
							<p class="prev-indent-bot">&copy; 2012 Interior <a rel="nofollow">Website Design &amp; 
                                Developed</a> by Shayan Anwar.</p>
							Phone: +92-323-3331729 Email:<a href="#">Shayananvar@hotmail.com</a>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	                
                        </form>
	</footer>
	<script type="text/javascript">	    Cufon.now(); </script>
</body>
</html>
