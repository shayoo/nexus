﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
namespace Nexus
{
    public partial class Services : System.Web.UI.Page
    {
        private SqlConnection xConn;
        private SqlDataAdapter xDataAdapter;
        private DataSet xDataSet;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);
            xDataSet = new DataSet();
            xDataAdapter = new SqlDataAdapter("select Services from tblservices where Status='true'", xConn);
            xDataAdapter.Fill(xDataSet);
            DGV1.DataSource = xDataSet.Tables[0];
            DGV1.DataBind();
        }
        private string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }

        protected void gridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DGV1.PageIndex = e.NewPageIndex;
            DGV1.DataBind();
        }

        protected void gridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dataTable = DGV1.DataSource as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

                DGV1.DataSource = dataView;
                DGV1.DataBind();
            }
        }
        protected void gridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DGV2.PageIndex = e.NewPageIndex;
            DGV2.DataBind();
        }

        protected void gridView2_Sorting(object sender, GridViewSortEventArgs e)
        {
            dl1.Dispose();
            
            DataTable dataTable = DGV2.DataSource as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

                DGV2.DataSource = dataView;
                DGV2.DataBind();
            }
        }

        protected void DGV1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = DGV1.SelectedRow;
                //Message.Text = "You selected " + row.Cells[2].Text + ".";
                DataSet xDataSet2 = new DataSet();
                SqlDataAdapter xDataAdapter2 = new SqlDataAdapter("select Package from tblinternet where Status='true' and Services='" + row.Cells[1].Text.ToString() + "'", xConn);
                xDataAdapter2.Fill(xDataSet2);
                DGV2.DataSource = xDataSet2.Tables[0];
                DGV2.DataBind();
            }
            catch (SqlException q)
            {
                Response.Write("<script>alert('There is no data like that')</script>");
            }
        }

        protected void DGV2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = DGV1.SelectedRow;
                GridViewRow row2 = DGV2.SelectedRow;
                //Message.Text = "You selected " + row.Cells[2].Text + ".";

                DataSet xDataSet3 = new DataSet();
                SqlDataAdapter xDataAdapter3 = new SqlDataAdapter("select types as Category,validity,Description from tblinternet where Status='true' and Services='" + row.Cells[1].Text.ToString() + "' and Package='" + row2.Cells[1].Text.ToString() + "'", xConn);
                xDataAdapter3.Fill(xDataSet3);
                dl1.DataSource = xDataSet3.Tables[0];
                dl1.DataBind();
                dl1.Dispose();
            }
            catch (SqlException q)
            {
                Response.Write("<script>alert('There is no data like that')</script>");
            }
        }

        protected void DGV1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //dl1.Visible = false;
            try
            {
                dl1.DataSource = null;
                dl1.DataBind();
                dl1.Dispose();
            }
            catch (SqlException q)
            {
                Response.Write("<script>alert('There is no data like that')</script>");
            }
            
            
        }

        
        //protected void gridView1_gridView1_SelectedIndex(object sender, EventArgs e)
        //{
        //    GridViewRow row = DGV1.SelectedRow;
        //    //Message.Text = "You selected " + row.Cells[2].Text + ".";
        //     DataSet xDataSet2 = new DataSet();
        //    SqlDataAdapter xDataAdapter2 = new SqlDataAdapter("select Package from tblinternet where Status='true' and Services='"+row.Cells[0].Text.ToString()+"'", xConn);
        //    xDataAdapter2.Fill(xDataSet2);
        //    DGV2.DataSource = xDataSet2.Tables[0];
        //    DGV2.DataBind();
        //}
 
    }
}
