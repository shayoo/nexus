﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
namespace Nexus
{
    public partial class View : System.Web.UI.Page
    {
        private SqlConnection xConn;
        private SqlDataAdapter xDataAdapter;
        private DataSet xDataSet;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["e"] == null)
            {
                Response.Redirect("Sorry.aspx");
            }
            else
            {

            }

            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);
            Show();            
        }
        private string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }

        protected void gridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DGV1.PageIndex = e.NewPageIndex;
            DGV1.DataBind();
        }

        protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dataTable = DGV1.DataSource as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

                DGV1.DataSource = dataView;
                DGV1.DataBind();
            }
        }

        protected void DGV1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //int a=Convert.ToInt32();
            xConn.Open();
            SqlCommand xcomd = new SqlCommand("update tblcontact set Status='false' where CID=" + DGV1.Rows[e.RowIndex].Cells[1].Text, xConn);
            xcomd.ExecuteNonQuery();
            xcomd.Dispose();
            xConn.Close();
            Show();
        }
        protected void Show()
        {
            xDataSet = new DataSet();
            xDataAdapter = new SqlDataAdapter("select CID,Uname as Username,email,phone,messagess from tblcontact where Status='true'", xConn);
            xDataAdapter.Fill(xDataSet);
            DGV1.DataSource = xDataSet.Tables[0];
            DGV1.DataBind();
        }
    }
}