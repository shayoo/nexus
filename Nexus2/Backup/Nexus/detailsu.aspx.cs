﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
namespace Nexus
{
    public partial class detailsu : System.Web.UI.Page
    {
        private SqlConnection xConn;
        private SqlDataAdapter xDataAdapter;
        private DataSet xDataSet;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["a"] == null && Session["e"] == null)
            {
                Response.Redirect("Sorry.aspx");
            }
            else
            {

            }
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);
        }

        protected void btndel_Click(object sender, EventArgs e)
        {
            xDataSet = new DataSet();
            xDataAdapter = new SqlDataAdapter("select Fname,Lname,Services,Package,Category,Bill,Date_of_join,U_ID,password from tblUser where U_ID='"+txtUID.Text+"' and Status='true'", xConn);
            xDataAdapter.Fill(xDataSet);
            DGV1.DataSource = xDataSet.Tables[0];
            DGV1.DataBind();
            txtUID.Text = "";
        }
    }
}
