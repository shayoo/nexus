﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
namespace Nexus
{
    public partial class Adduser : System.Web.UI.Page
    {
        private SqlConnection xConn;
        int i;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["a"] == null && Session["e"] == null)
            {
                Response.Redirect("Sorry.aspx");
            }
            
            else
            {

            }
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);
            Show();
            
            xConn.Open();
            SqlCommand xcomd3 = new SqlCommand("select COUNT(*) from tblUser where Status='true'", xConn);
            SqlDataReader ds3;
            ds3 = xcomd3.ExecuteReader();
            while (ds3.Read())
            {
                i = Convert.ToInt32(ds3[0].ToString()) + 1;
            }
            ds3.Close();
            xcomd3.Dispose();
            txtUID.Text = "PKR / USER / " + i;
            txtUID.Enabled = false;
            xConn.Close();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (dll1.SelectedIndex.Equals(0))
            {
                l2.ForeColor = System.Drawing.Color.Red;
                l2.Text = "You Should Select Service!";
                
            }
            else if (dll2.SelectedIndex.Equals(0))
            {
                l1.ForeColor = System.Drawing.Color.Red;
                l1.Text = "You Should Select Package!";
            }
            else if (ddl3.SelectedIndex.Equals(0))
            {
                l1.ForeColor = System.Drawing.Color.Red;
                l1.Text = "You Should Select Category!";
            }
            else
            {
                try
                {
                    xConn.Open();
                    SqlCommand xcomd = new SqlCommand("insert into tblUser values('" + txtfname.Text + "','" + txtlname.Text + "','" + dll1.SelectedItem.Text.ToString() + "','" + dll2.SelectedItem.Text.ToString() + "','" + ddl3.SelectedItem.Text.ToString() + "','" + txtemail.Text + "','" + txtbill.Text + "$','" + DateTime.Today.ToString("dd/MM/yyyy") + "','" + txtUID.Text  + "','" + txtpass.Text + "','true','Unpaid')", xConn);
                    SqlCommand xcomd2 = new SqlCommand("insert into tblusers values('" + txtUID.Text + "','"+txtpass.Text+"','true')", xConn);
                    xcomd.ExecuteNonQuery();
                    xcomd2.ExecuteNonQuery();
                    xcomd.Dispose();
                    xcomd2.Dispose();
                    xConn.Close();
                    Response.Write("<script>alert('Data Saved!')</script>");
                    Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); },0)</script>");
                    dll1.SelectedIndex=0;
                    dll2.SelectedIndex=0;
                    ddl3.SelectedIndex=0;
                    txtbill.Text = "";
                    txtUID.Text = "";
                    txtpass.Text = "";
                    txtemail.Text = "";
                    txtfname.Text = "";
                    txtlname.Text = "";
                    
                }
                catch (SqlException sq)
                {
                    Response.Write("<script>alert('UID is already exist!')</script>");
                    txtfname.Text = "";
                    txtlname.Text = "";
                    dll1.SelectedIndex = 0;
                    dll2.SelectedIndex = 0;
                    ddl3.SelectedIndex = 0;
                    txtbill.Text = "";
                    txtUID.Text = "";
                    txtpass.Text = "";
                    txtemail.Text = "";
                }
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            txtfname.Text = "";
            txtlname.Text = "";
            dll1.SelectedIndex.Equals(0);
            dll2.SelectedIndex.Equals(0);
            ddl3.SelectedIndex.Equals(0);
            txtbill.Text = "";
            txtUID.Text = "";
            txtpass.Text = "";
            txtemail.Text = "";

        }


        protected void Show()
        {


            try
            {
                xConn.Open();
                //DataTable dr = new DataTable();

                SqlCommand cmd = new SqlCommand("select Services from tblservices where Status='true'", xConn);
                //cmd.Fill(dr);
                //cmd.CommandType = CommandType.Text;

                SqlDataReader ds;
                ds = cmd.ExecuteReader();
                while (ds.Read())
                {
                    dll1.Items.Add(ds[0].ToString());
                }
                //ListItem li = new ListItem();
                //for (int i = 0; i <= str; i++)
                //{
                //  li.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                //li.Value = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                //dll1.DataSource = ds;
                //dll1.DataTextField = "Karachi";
                //dll1.DataValueField = "Karachi";
                //dll1.Items.Add(li);
                //}
                //dll1.DataBind();
                //dr.Clear();
                cmd.Dispose();
                ds.Dispose();
                ds.Close();
                xConn.Close();
                


            }
            catch (IndexOutOfRangeException se)
            {
                Response.Write("<script>alert('Please enter correctly')</script>");
                Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); parent.location ='AHome.aspx'; },0)</script>");
            }
            catch (SqlException sq)
            {
                Response.Write("<script>alert('Please enter correctly')</script>");
            }
            //dll1.Items.Insert(0, new ListItem("    :: Select Franchise ::   ", "0"));
        }

        protected void dll1_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                xConn.Open();
                //DataTable dr = new DataTable();

                SqlCommand cmd = new SqlCommand("select Package from tblinternet where Status='true' and Services='" + dll1.SelectedItem.Text.ToString() + "'", xConn);                //cmd.Fill(dr);
                //cmd.CommandType = CommandType.Text;

                SqlDataReader ds;
                ds = cmd.ExecuteReader();
                while (ds.Read())
                {
                    dll2.Items.Add(ds[0].ToString());
                }
                //ListItem li = new ListItem();
                //for (int i = 0; i <= str; i++)
                //{
                //  li.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                //li.Value = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                //dll1.DataSource = ds;
                //dll1.DataTextField = "Karachi";
                //dll1.DataValueField = "Karachi";
                //dll1.Items.Add(li);
                //}
                //dll1.DataBind();
                //dr.Clear();
                cmd.Dispose();
                ds.Dispose();
                xConn.Close();
                


            }
            catch (IndexOutOfRangeException se)
            {
                Response.Write("<script>alert('Please enter correctly')</script>");
                Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); parent.location ='AHome.aspx'; },0)</script>");
            }
            catch (SqlException sq)
            {
                Response.Write("<script>alert('Please enter correctly')</script>");
            }
            //dll1.Items.Insert(0, new ListItem("    :: Select Franchise ::   ", "0"));

        }

        protected void dll2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            try
            {
                xConn.Open();
                //DataTable dr = new DataTable();

                SqlCommand cmd2 = new SqlCommand("select types from tblinternet where Status='true' and Services='" + dll1.SelectedItem.Text.ToString() + "' and Package='"+dll2.SelectedItem.Text.ToString()+"'", xConn);                //cmd.Fill(dr);
                //cmd.CommandType = CommandType.Text;

                SqlDataReader ds2;
                ds2 = cmd2.ExecuteReader();
                while (ds2.Read())
                {
                    ddl3.Items.Add(ds2[0].ToString());
                }
                //ListItem li = new ListItem();
                //for (int i = 0; i <= str; i++)
                //{
                //  li.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                //li.Value = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                //dll1.DataSource = ds;
                //dll1.DataTextField = "Karachi";
                //dll1.DataValueField = "Karachi";
                //dll1.Items.Add(li);
                //}
                //dll1.DataBind();
                //dr.Clear();
                cmd2.Dispose();
                ds2.Dispose();
                xConn.Close();
                xConn.Dispose();

            }
            catch (IndexOutOfRangeException se)
            {
                Response.Write("<script>alert('Please enter correctly')</script>");
                Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); parent.location ='AHome.aspx'; },0)</script>");
            }
            catch (SqlException sq)
            {
                Response.Write("<script>alert('Please enter correctly')</script>");
            }
            //dll1.Items.Insert(0, new ListItem("    :: Select Franchise ::   ", "0"));

        }


        

    }
}
