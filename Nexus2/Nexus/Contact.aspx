﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Nexus.Contact" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Contacts</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">  
	<script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="js/cufon-yui.js" type="text/javascript"></script>
	<script src="js/cufon-replace.js" type="text/javascript"></script>
	<script src="js/Vegur_500.font.js" type="text/javascript"></script>
	<script src="js/Ropa_Sans_400.font.js" type="text/javascript"></script> 
	<script src="js/FF-cash.js" type="text/javascript"></script>	
	<script src="js/script.js" type="text/javascript"></script>  
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
		</a>
	</div>
	<![endif]-->
	<!--[if lt IE 9]>
 		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
	<![endif]-->
</head>
<body id="page5">
	<!--==============================header=================================-->
	<header>
		<div class="border-bot">
			<div class="main">
				<h1><a href="index.html">InternetCafe</a></h1>
				<nav>
					<ul class="menu">
						<li><a href="Default.aspx">Home</a></li>
						<%--<li><a href="index-1.html">Internet</a></li>--%>
						<li><a href="Services.aspx">Services</a></li>
						<li><a href="Franchise.aspx">Franchise</a></li>
						<li><a class="active" href="Contact.aspx">Contacts</a></li>
					</ul>
				</nav>
				<div class="clear"></div>
			</div>
			</div>
	</header>
	<!--==============================content================================-->
	<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com - Mrach 03, 2012!</div>
		<div class="main">
			<div class="container_12">
				<div class="wrapper">
					<article class="grid_8">
						<h3>Contact / Order Form</h3>
						<form id="contact-form" method="post" enctype="multipart/form-data">
						<form id="f1" runat="server">
							<fieldset>
								  <label><span class="text-form">Name:</span><asp:TextBox ID="txtuname" runat="server" TextMode="SingleLine" TabIndex="1"></asp:TextBox>
								  <asp:RequiredFieldValidator ID="RFV1" ControlToValidate="txtuname" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
								  <asp:RegularExpressionValidator ID="REV3" runat="server" ValidationExpression="^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$" ControlToValidate="txtuname" ErrorMessage="Invalid Name Format"></asp:RegularExpressionValidator> 
								  </label>
								  <label><span class="text-form">Email:</span><asp:TextBox ID="txtemail" runat="server" TextMode="SingleLine" TabIndex="2"></asp:TextBox>
								  <asp:RequiredFieldValidator ID="RFV2" ControlToValidate="txtemail" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
								  <asp:RegularExpressionValidator ID="rev1" runat="server" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ControlToValidate="txtemail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator> </label>							  
								  <label><span class="text-form">Phone:</span><asp:TextBox ID="txtphone" runat="server" TextMode="SingleLine" TabIndex="3"></asp:TextBox>
								  <asp:RequiredFieldValidator ID="RFV3" ControlToValidate="txtphone" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
								  <asp:RegularExpressionValidator ID="REV2" runat="server" ValidationExpression="^\d+$" ControlToValidate="txtphone" ErrorMessage="Invalid Phone Format"></asp:RegularExpressionValidator> </label>							  
								  <div class="wrapper">
									<div class="text-form">Message:</div>
									<div class="extra-wrap">
										<asp:TextBox ID="txtmess" runat="server" TextMode="MultiLine" TabIndex="4"></asp:TextBox>
										<asp:RequiredFieldValidator ID="RFV4" ControlToValidate="txtmess" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
										
								  <br />
										<asp:Button ID="btnclear" runat="server" Text="Clear" TabIndex="5" Width="60px" 
                                            onclick="btnclear_Click" />
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:Button ID="btnsend" runat="server" Text="Send" TabIndex="6" Width="60px" 
                                            onclick="btnsend_Click" />
									</div>
								  </div>							
							</fieldset>
							</form>											
						</form>
					</article>
					<article class="grid_4">
						<div class="indent-top indent-left">
							<div class="wrapper p3">
								<figure class="img-indent-r"><a href="#"><img src="images/page1-img1.png" alt=""></a></figure>
								<div class="extra-wrap">
									<strong class="title-1">Tell Your<strong>Friends</strong><em>About</em><em>Our</em><em>Services</em></strong>
								</div>
							</div>
							<h3 class="p1">Latest News</h3>
							<p class="prev-indent-bot">24 Hour Emergency Towing</p>
							<p class="p0">Monday - Friday: 7:30 am - 6:00</p>
							<p class="prev-indent-bot">Saturday: 7:30 am - Noon</p>
							<p class="img-indent-bot">Night Drop Available</p>
							<dl>
								<dt class="prev-indent-bot">Demolink.org 8901 Marmora Road, Glasgow, D04 89GR.</dt>
								<dd><span>Telephone:</span>+1 959 552 5963;</dd>
								<dd><span>FAX:</span>+1 959 552 5963</dd>
								<dd><span>E-mail:</span><a href="#">mail@demolink.org</a></dd>
							</dl>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>
	<!--==============================footer=================================-->
	<footer>
			<div class="main">
			<div class="container_12">
				<div class="wrapper">
					<div class="grid_3">
						<div class="spacer-1">
							<a href="Default.aspx"><img src="images/nex5.jpg" alt=""></a>
						</div>
					</div>
					<div class="grid_5">
						<div class="indent-top2">
							<p class="prev-indent-bot">&copy; 2012 Interior <a rel="nofollow">Website Design &amp; 
                                Developed</a> by Shayan Anwar.</p>
							Phone: +92-323-3331729 Email:<a href="#">Shayananvar@hotmail.com</a>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	
	</footer>
	<script type="text/javascript"> Cufon.now(); </script>
</body>
</html>
