﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Services.aspx.cs" Inherits="Nexus.Services" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    	<title>Services</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">  
	<script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="js/cufon-yui.js" type="text/javascript"></script>
	<script src="js/cufon-replace.js" type="text/javascript"></script>
	<script src="js/Vegur_500.font.js" type="text/javascript"></script>
	<script src="js/Ropa_Sans_400.font.js" type="text/javascript"></script> 
	<script src="js/FF-cash.js" type="text/javascript"></script> 
	<script src="js/script.js" type="text/javascript"></script> 
	
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
		</a>
	</div>
	<![endif]-->
	<!--[if lt IE 9]>
 		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
	<![endif]-->

</head>
<body id="page3">
	<!--==============================header=================================-->
	<header>
		<div class="border-bot">
			<div class="main">
				<h1><a href="Default.aspx">InternetCafe</a></h1>
				<nav>
					<ul class="menu">
						<li><a href="Default.aspx">Home</a></li>
						<%--<li><a href="index-1.html">Internet</a></li>--%>
						<li><a class="active" href="Services.aspx">Services</a></li>
						<li><a href="Franchise.aspx">Franchise</a></li>
						<li><a href="Contact.aspx">Contacts</a></li>
					</ul>
				</nav>
				<div class="clear"></div>
			</div>
			</div>
	</header>
	
	<!--==============================content================================-->
	<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com - Mrach 03, 2012!</div>
				
				
				<form id="f3" runat="server">
		<div class="main" style="height:700px;">
			<div class="container_12">
			
				<div class="wrapper">
				<h3>Our Services</h3><br />
				
				
				        <asp:GridView ID="DGV1" runat="server" AllowPaging="True" AllowSorting="True" 
                            AutoGenerateSelectButton="True" Height="150px" HorizontalAlign="Left" 
                            Width="259px" OnPageIndexChanging="gridView1_PageIndexChanging" 
                            OnSorting="gridView1_Sorting" 
                            onselectedindexchanged="DGV1_SelectedIndexChanged" 
                            onselectedindexchanging="DGV1_SelectedIndexChanging">
                            <RowStyle HorizontalAlign="Center" Wrap="False" />
                            <PagerStyle HorizontalAlign="Center" />
                        </asp:GridView>
                        <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         <asp:GridView ID="DGV2" runat="server" AllowPaging="True" AllowSorting="True" 
                            AutoGenerateSelectButton="True" Height="150px" HorizontalAlign="Left" 
                            Width="259px" OnPageIndexChanging="gridView2_PageIndexChanging" 
                                            OnSorting="gridView2_Sorting" 
                                            onselectedindexchanged="DGV2_SelectedIndexChanged">
                            <RowStyle HorizontalAlign="Center" Wrap="False" />
                            <PagerStyle HorizontalAlign="Center" />
                        </asp:GridView>

                        
				                        <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:DetailsView ID="dl1" runat="server" CellPadding="4" 
                            ForeColor="#333333" GridLines="None" Height="300px" Width="400px">
                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
                                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" HorizontalAlign="Center" />
                                            <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <EditRowStyle BackColor="#999999" />
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        </asp:DetailsView>
				
				
				
				</div>
			</div>
		</div>
	</section>
	
	<!--==============================footer=================================-->
	<footer>
		<div class="main">
			<div class="container_12">
				<div class="wrapper">
					<div class="grid_3">
						<div class="spacer-1">
							<a href="index.html"><img src="images/nex5.jpg" alt=""></a>
						</div>
					</div>
					<div class="grid_5">
						<div class="indent-top2">
							<p class="prev-indent-bot">&copy; 2012 Interior " href="http://www.templatemonster.com/" target="_blank">Website Template</a> by TemplateMonster.com</p>
							Phone: +1 800 559 6580 Email: <a href="#">info@internetcafe.com</a>
						</div>
					</div>
					<div class="grid_4">
						<ul class="list-services">
							<li><asp:HyperLink ID="h1" runat="server" NavigateUrl="http://www.facebook.com/" Target="_blank" class="item-1"></asp:HyperLink></li>
							<li><asp:HyperLink ID="h2" runat="server" NavigateUrl="http://www.twitter.com/" Target="_blank" class="item-2"></asp:HyperLink></li>
							<li><asp:HyperLink ID="h3" runat="server" NavigateUrl="https://plus.google.com/" Target="_blank" class="item-3"></asp:HyperLink></li>
							<li><asp:HyperLink ID="h4" runat="server" NavigateUrl="https://www.youtube.com/" Target="_blank" class="item-4"></asp:HyperLink></li>
						</ul>
						<span class="footer-text">&copy; 2012 <a class="link color-2" href="#">Privacy Policy</a></span>
					</div>
				</div>
			</div>
		</div>

                        
				</form>
				
				
				
	</footer>
	<script type="text/javascript"> Cufon.now(); </script>
</form>
</body>
</html>
