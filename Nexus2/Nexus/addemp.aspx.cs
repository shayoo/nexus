﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
namespace Nexus
{
    public partial class addemp : System.Web.UI.Page
    {
        private SqlConnection xConn;
        int i;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["a"] == null)
            {
                Response.Redirect("Sorry.aspx");
            }
            else
            {

            }
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);
            Show();
            xConn.Open();
            SqlCommand xcomd3 = new SqlCommand("select COUNT(*) from tblemp where Status='true'", xConn);
            SqlDataReader ds3;
            ds3 = xcomd3.ExecuteReader();
            while (ds3.Read())
            {
                i = Convert.ToInt32(ds3[0].ToString()) + 1;
            }
            ds3.Close();
            xcomd3.Dispose();
            txtUID.Text = "PKR / EMP / " + i;
            txtUID.Enabled = false;
            xConn.Close();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (dll1.SelectedIndex.Equals(0))
            {
                l1.ForeColor = System.Drawing.Color.Red;
                l1.Text = "You Should Select Service!";

            }
            else
            {
                try
                {
                    xConn.Open();
                    SqlCommand xcomd = new SqlCommand("insert into tblemp values('" + txtfname.Text + "','" + txtlname.Text + "','" + txtphone.Text + "','" + txtadd.Text + "','" + txtemail.Text + "','" + txtbill.Text + "$','" + dll1.SelectedItem.Text.ToString() + "','"+txtdes.Text+"','" + DateTime.Today.ToString("dd/MM/yyyy") + "','" + txtUID.Text + "','" + txtpass.Text + "','true')", xConn);
                    //SqlCommand xcomd2 = new SqlCommand("insert into tblinternet values('" + txtpk.Text + "','','','','','','true')", xConn);
                    xcomd.ExecuteNonQuery();
                    //xcomd2.ExecuteNonQuery();
                    xcomd.Dispose();
                    //xcomd2.Dispose();
                    xConn.Close();
                    Response.Write("<script>alert('Data Saved!')</script>");
                    Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); },0)</script>");
                    dll1.SelectedIndex=0;
                    txtfname.Text = "";
                    txtlname.Text = "";
                    txtadd.Text = "";
                    txtbill.Text = "";
                    txtphone.Text = "";
                    txtUID.Text = "";
                    txtpass.Text = "";
                    txtdes.Text = "";
                    txtemail.Text = "";

                }
                catch (SqlException sq)
                {
                    Response.Write("<script>alert('UID is already exist!')</script>");
                    dll1.SelectedIndex=0;
                    txtfname.Text = "";
                    txtlname.Text = "";
                    txtadd.Text = "";
                    txtbill.Text = "";
                    txtphone.Text = "";
                    txtUID.Text = "";
                    txtpass.Text = "";
                    txtemail.Text = "";
                    txtdes.Text = "";                    
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            txtfname.Text = "";
            txtlname.Text = "";
            txtadd.Text = "";
            txtbill.Text = "";
            txtphone.Text = "";
            txtUID.Text = "";
            txtpass.Text = "";
            dll1.SelectedIndex=0;
            txtdes.Text = "";                    

        }
        protected void Show()
        {


            try
            {
                xConn.Open();
                //DataTable dr = new DataTable();

                SqlCommand cmd = new SqlCommand("select Karachi from tblfranchise where Status='true'", xConn);
                //cmd.Fill(dr);
                //cmd.CommandType = CommandType.Text;

                SqlDataReader ds;
                ds = cmd.ExecuteReader();
                while (ds.Read())
                {
                    dll1.Items.Add(ds[0].ToString());
                }
                //ListItem li = new ListItem();
                //for (int i = 0; i <= str; i++)
                //{
                //  li.Text = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                //li.Value = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                //dll1.DataSource = ds;
                //dll1.DataTextField = "Karachi";
                //dll1.DataValueField = "Karachi";
                //dll1.Items.Add(li);
                //}
                //dll1.DataBind();
                //dr.Clear();
                cmd.Dispose();
                ds.Dispose();
                ds.Close();
                xConn.Close();


            }
            catch (IndexOutOfRangeException se)
            {
                Response.Write("<script>alert('Please enter correctly')</script>");
                Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); parent.location ='AHome.aspx'; },0)</script>");
            }
            catch (SqlException sq)
            {
                Response.Write("<script>alert('Please enter correctly')</script>");
            }
            //dll1.Items.Insert(0, new ListItem("    :: Select Franchise ::   ", "0"));
        }
    }
}
