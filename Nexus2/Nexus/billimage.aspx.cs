﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
namespace Nexus
{
    public partial class billimage : System.Web.UI.Page
    {
        private SqlConnection xConn;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["e"] == null)
            {
                Response.Redirect("Sorry.aspx");
            }
            else
            {

            }
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);
            
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string FName = UFile.FileName.ToString();
                //int FSize = UFile.PostedFile.ContentLength;
                UFile.PostedFile.SaveAs(Server.MapPath("Images/" + FName));
                //string NewFName = txtpk.Text;
                //System.IO.File.Move(Server.MapPath("Images/" + FName), Server.MapPath("Images/" + NewFName));
                //          
                xConn.Open();
                SqlCommand xcomd = new SqlCommand("update tblbill set Images='" + FName +"'", xConn);
                //SqlCommand xcomd2 = new SqlCommand("insert into tblinternet values('" + txtpk.Text + "','','','','','','true')", xConn);
                xcomd.ExecuteNonQuery();
                //xcomd2.ExecuteNonQuery();
                xcomd.Dispose();
                //xcomd2.Dispose();
                xConn.Close();
                Response.Write("<script>alert('Bill Image has been saved!')</script>");
                Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); },0)</script>");

            }
            catch (SqlException sq)
            {
                Response.Write("<script>alert('please choose file by browsing!')</script>");

            }
            catch (IOException ie)
            {
                Response.Write("<script>alert('please choose file by browsing!')</script>");

            }
        }
    }
}
