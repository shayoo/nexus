﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
namespace Nexus
{
    public partial class deluser : System.Web.UI.Page
    {
        private SqlConnection xConn;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["a"] == null && Session["e"] == null)
            {
                Response.Redirect("Sorry.aspx");
            }

            else
            {

            }
            string sString = ConfigurationManager.ConnectionStrings["xConn"].ConnectionString;
            xConn = new SqlConnection(sString);
        }

        protected void btndel_Click(object sender, EventArgs e)
        {
try
            {
                xConn.Open();
                SqlCommand sc = new SqlCommand("update tblUser set Status='false' where U_ID='" + txtUID.Text.ToString() + "'", xConn);
                SqlCommand sc2 = new SqlCommand("update tblusers set Status='false' where Uname='" + txtUID.Text.ToString() + "'", xConn);

                sc.ExecuteNonQuery();
                sc2.ExecuteNonQuery();

                sc.Dispose();
                sc2.Dispose();
                
                xConn.Close();
                Response.Write("<script>alert('Deleted!')</script>");
                Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); parent.location ='AHome.aspx'; },0)</script>");
            }
catch (SqlException s)
{
    Response.Write("<script>alert('There is no Package')</script>");
}
try
{
    xConn.Open();
    SqlCommand sc3 = new SqlCommand("update tblpayment set Status='false' where U_ID='" + txtUID.Text.ToString() + "'", xConn);
    sc3.ExecuteNonQuery();
    sc3.Dispose();
    xConn.Close();
    Response.Write("<script>setTimeout(function(){parent.$.fancybox.close(); parent.location ='AHome.aspx'; },0)</script>");
}
catch (SqlException s)
{
    Response.Write("<script>alert('There is no bill made for this user yet!')</script>");
}
        }
    }
}
