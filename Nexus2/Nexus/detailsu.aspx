﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="detailsu.aspx.cs" Inherits="Nexus.detailsu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>User Details</title>
    <style type="text/css">
        .style1
        {
            width: 92px;
        }
        .style2
        {
            width: 136px;
        }
    </style>
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">  
	<script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="js/cufon-yui.js" type="text/javascript"></script>
	<script src="js/cufon-replace.js" type="text/javascript"></script>
	<script src="js/Vegur_500.font.js" type="text/javascript"></script>
	<script src="js/Ropa_Sans_400.font.js" type="text/javascript"></script> 
	<script src="js/FF-cash.js" type="text/javascript"></script>	
	<script src="js/script.js" type="text/javascript"></script>  
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
		</a>
	</div>
	<![endif]-->
	<!--[if lt IE 9]>
 		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
	<![endif]-->
	
</head>
<body id="page5">
	<!--==============================header=================================-->
	<header>
		<div class="border-bot">
			<div class="main">
				<h1><a href="index.html">InternetCafe</a></h1>
				<nav>
					
				</nav>
				<div class="clear"></div>
			</div>
			</div>
	</header>
	<!--==============================content================================-->
	<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com - Mrach 03, 2012!</div>
						<form id="f3" runat="server">
                        
		<div class="main" style="height:400px;">
			<div class="container_12">
				<div class="wrapper">
					<article class="grid_8">
						<h3>User Details</h3>
						
						<table style="width: 281px; margin-left:180px;">
            <tr>
    <td class="style1">Enter UserID :</td>
    <td class="style2"><asp:TextBox ID="txtUID" runat="server"></asp:TextBox> 
     <asp:RequiredFieldValidator ID="RFV6" ControlToValidate="txtUID" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
     </td>
     <td>
                    <asp:Button ID="Button1" runat="server" onclick="btndel_Click" Text="GO" />
                </td>
    
    </table>

                        <br />

						                <asp:GridView ID="DGV1" runat="server" BackColor="White" 
                                            BorderColor="#DEDFDE" BorderStyle="None" 
                        BorderWidth="1px" CellPadding="4" 
                                            ForeColor="Black" GridLines="Vertical" Height="16px" HorizontalAlign="Center" 
                                            Width="956px" PageSize="1" AllowSorting="True">
                                            <FooterStyle BackColor="#CCCC99" />
                                            <RowStyle BackColor="#F7F7DE" HorizontalAlign="Center" />
                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center" />
                                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                        </asp:GridView>
					</article>
					<article class="grid_4">
						<div class="indent-top indent-left">
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>
	<!--==============================footer=================================-->
	<footer>
		<div class="main">
			<div class="container_12">
				<div class="wrapper">
					<div class="grid_3">
						<div class="spacer-1">
							<a href="Default.aspx"><img src="images/nex5.jpg" alt=""></a>
						</div>
					</div>
					<div class="grid_5">
						<div class="indent-top2">
							<p class="prev-indent-bot">&copy; 2012 Interior <a rel="nofollow" href="http://www.templatemonster.com/" target="_blank">Website Template</a> by TemplateMonster.com</p>
							Phone: +1 800 559 6580 Email: <a href="#">info@internetcafe.com</a>
						</div>
					</div>
					
				</div>
			</div>
		</div>
                        
                        </form>
	</footer>
	<script type="text/javascript">	    Cufon.now(); </script>
</body>
</html>
