﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="editfran.aspx.cs" Inherits="Nexus.editfran" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Edit Franchise</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Editing Franchise</h3>
    <table style="width: 529px; margin-left:140px;">
            <tr>
                <td class="style1">
                    Select Franchise:</td>
                <td>
                    <asp:DropDownList ID="dll1" runat="server" Height="28px" Width="189px">
                        <asp:ListItem> :: Select Franchise ::</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="l1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Rename Franchise:</td>
                <td>
                    <asp:TextBox ID="txtpk" runat="server" Width="189px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFV1" ControlToValidate="txtpk" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="REV5" runat="server" ValidationExpression="^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$" ControlToValidate="txtpk" ErrorMessage="Invalid Place Format"></asp:RegularExpressionValidator> 
                </td>
            </tr>
            <tr>
                <td class="style1">
                    New
                    Address:</td>
                <td>
                    <asp:TextBox ID="txtadd" runat="server" Width="188px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFV2" ControlToValidate="txtadd" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    New
                    Supervisor:</td>
                <td>
                    <asp:TextBox ID="txtsup" runat="server" Width="188px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFV3" ControlToValidate="txtsup" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="REV2" runat="server" ValidationExpression="^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$" ControlToValidate="txtsup" ErrorMessage="Invalid Name Format"></asp:RegularExpressionValidator> 
                </td>
            </tr>
            <tr>
                <td class="style1">
                    New
                    Contact No:</td>
                <td>
                    <asp:TextBox ID="txtcn" runat="server" Width="187px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFV5" ControlToValidate="txtcn" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="REV3" runat="server" ValidationExpression="^\d+$" ControlToValidate="txtcn" ErrorMessage="Invalid Phone Format"></asp:RegularExpressionValidator>
                    
                </td>
            </tr>
            <tr>
                <td class="style1">
                    New
                    Email:</td>
                <td>
                    <asp:TextBox ID="txtemail" runat="server" Width="187px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFV4" ControlToValidate="txtemail" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
				    <asp:RegularExpressionValidator ID="REV4" runat="server" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ControlToValidate="txtemail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator> 
                </td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnsave" runat="server" onclick="btnsave_Click" Text="Save" />
&nbsp;
                    <asp:Button ID="btnclear" runat="server" onclick="btnclear_Click" 
                        Text="Clear" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
