﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Nexus.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Login</title>
    
</head>
<body>
    
    <form id="form1" runat="server" 
    style="margin-top:0px;">
    
    &nbsp;<div style="margin-top:0px;">
      
         <asp:Login ID="Login1" runat="server" BackColor="#F7F6F3" BorderColor="#E6E2D8" 
            BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
            Font-Size="0.8em" ForeColor="#333333" Height="190px" Width="361px" 
            onauthenticate="Login1_Authenticate" style="margin-left:170px;" 
             RememberMeSet="True">
            <CheckBoxStyle Font-Bold="True" Font-Size="1.8em" />
            <TextBoxStyle Font-Size="1.2em" Height="23px" Width="180px" />
            <LoginButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                BorderWidth="1px" Font-Names="Verdana" Font-Size="1.4em" ForeColor="#284775" 
                Height="25px" Width="70px" />
            <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
            <LabelStyle Font-Size="1.3em" />
            <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="1.9em" 
                ForeColor="White" />
        </asp:Login>
    
    </div>
    </form>
</body>
</html>
