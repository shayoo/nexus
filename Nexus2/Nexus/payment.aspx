﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="payment.aspx.cs" Inherits="Nexus.payment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
        <title>Bill Payment</title>
    <style type="text/css">
        .style2
        {
            width: 136px;
        }
        .floating_label {
  position:absolute; z-index:200; top:178px; left:260px;
}
.floating_label2 {
  position:absolute; z-index:200; top:80px; left:180px;
}
.floating_label3 {
  position:absolute; z-index:200; top:125px; left:100px;
}
.floating_label4 {
  position:absolute; z-index:200; top:80px; left:500px;
}
.floating_label5 {
  position:absolute; z-index:200; top:230px; left:220px;
}
.floating_label6 {
  position:absolute; z-index:200; top:285px; left:220px;
}
.floating_label7 {
  position:absolute; z-index:200; top:320px; left:260px;
}

        .style3
        {
            width: 153px;
        }

    </style>
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">  
	<script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="js/cufon-yui.js" type="text/javascript"></script>
	<script src="js/cufon-replace.js" type="text/javascript"></script>
	<script src="js/Vegur_500.font.js" type="text/javascript"></script>
	<script src="js/Ropa_Sans_400.font.js" type="text/javascript"></script> 
	<script src="js/FF-cash.js" type="text/javascript"></script>	
	<script src="js/script.js" type="text/javascript"></script>  
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
		</a>
	</div>
	<![endif]-->
	<!--[if lt IE 9]>
 		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
	<![endif]-->

</head>
<body id="page5">
	<!--==============================header=================================-->
	<header>
		<div class="border-bot">
			<div class="main">
				<h1><a href="index.html">InternetCafe</a></h1>
				<nav>
					
				</nav>
				<div class="clear"></div>
			</div>
			</div>
	</header>
	<!--==============================content================================-->
	<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com - Mrach 03, 2012!</div>
						<form id="f3" runat="server">
                        
		<div class="main" style="height:900px;">
			<div class="container_12">
				<div class="wrapper">
					<article class="grid_8">
						<h3>Bill Payment</h3>
						
						<br />
						<div style="position:relative;">
  <asp:Image ID="image1" runat="server" />
  <asp:Label runat="server" id="lblTest" CssClass="floating_label" />
  <asp:Label runat="server" id="Label1" CssClass="floating_label2" />
  <asp:Label runat="server" id="Label2" CssClass="floating_label3" />
    <asp:Label runat="server" id="Label3" CssClass="floating_label4" />
      <asp:Label runat="server" id="Label4" CssClass="floating_label5" />
            <asp:Label runat="server" id="Label5" CssClass="floating_label6" />
                        <asp:Label runat="server" id="Label6" CssClass="floating_label7" />
</div>
                        <br />
                        <br />
						
						<table style="width: 450px; margin-left:220px;">
            <tr>
    <td class="style3">Enter UserID :</td>
    <td class="style2"><asp:TextBox ID="txtUID" runat="server"></asp:TextBox> 
     <%--<asp:RequiredFieldValidator ID="RFV6" ControlToValidate="txtUID" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>--%>
     </td>
     <td>
                    &nbsp;</td>
    </tr>
            <tr>
    <td class="style3"></td>
    <td>
                    <asp:Button ID="Button1" runat="server" Text="GO" 
            onclick="Button1_Click" CausesValidation="false" />
     </td></tr>
     
     <tr>
     <td></td>
     <td></td>
    </tr>
    <tr>
     <td></td>
     <td></td>
    </tr>
    <tr>
     <td class="style3"></td>
     <td></td>
    </tr>
         <tr>
     <td class="style3">Username:</td>
     <td><asp:TextBox ID="txtfname" runat="server"></asp:TextBox>
     <asp:RequiredFieldValidator ID="RFV1" ControlToValidate="txtfname" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
      <asp:RegularExpressionValidator ID="REV1" runat="server" ValidationExpression="^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$" ControlToValidate="txtfname" ErrorMessage="Invalid FName Format"></asp:RegularExpressionValidator> </td>
    </tr>
    
    <tr>
     <td class="style3">Payment:</td>
     <td><asp:TextBox ID="txtbill" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RFV5" ControlToValidate="txtbill" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="REV5" runat="server" ValidationExpression="^\d+$" ControlToValidate="txtbill" ErrorMessage="Invalid bill Format"></asp:RegularExpressionValidator></td>
    </tr>
    <tr>
     <td class="style3">Rupees in words:</td>
     <td><asp:TextBox ID="txtriw" runat="server"></asp:TextBox>
     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtriw" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
      <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$" ControlToValidate="txtriw" ErrorMessage="Invalid Format"></asp:RegularExpressionValidator> </td>
    </tr>
    <tr>
     <td class="style3">Mode of Payment:</td>
     <td><asp:TextBox ID="txtmop" runat="server"></asp:TextBox>
     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtmop" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator>
      <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$" ControlToValidate="txtmop" ErrorMessage="Invalid Format"></asp:RegularExpressionValidator> </td>
    </tr>
    <tr>
     <td>Note:</td>
     <td><asp:TextBox ID="txtnote" runat="server" TextMode="MultiLine"></asp:TextBox>
     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtnote" ErrorMessage="*" Display="Static" runat="server" ></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
     <td class="style3"></td>
     <td>
         <asp:Button ID="btnsave" runat="server" Text="Save" CausesValidation="true" 
             onclick="btnsave_Click" />
        </td>
    </tr>
    </table>

                        <br />

						                
					</article>
					<article class="grid_4">
						<div class="indent-top indent-left">
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>
	<!--==============================footer=================================-->
	<footer>
		<div class="main">
			<div class="container_12">
				<div class="wrapper">
					<div class="grid_3">
						<div class="spacer-1">
							<a href="Default.aspx"><img src="images/nex5.jpg" alt=""></a>
						</div>
					</div>
					<div class="grid_5">
						<div class="indent-top2">
							<p class="prev-indent-bot">&copy; 2012 Interior <a rel="nofollow" href="http://www.templatemonster.com/" target="_blank">Website Template</a> by TemplateMonster.com</p>
							Phone: +1 800 559 6580 Email: <a href="#">info@internetcafe.com</a>
						</div>
					</div>
				
				</div>
			</div>
		</div>
                        
                        </form>
	</footer>
	<script type="text/javascript">	    Cufon.now(); </script>
</body>
</html>
